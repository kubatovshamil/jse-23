package ru.t1.kubatov.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User Id is empty...");
    }

}
