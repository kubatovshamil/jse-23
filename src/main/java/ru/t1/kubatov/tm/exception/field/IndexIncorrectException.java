package ru.t1.kubatov.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
