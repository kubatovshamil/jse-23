package ru.t1.kubatov.tm.exception.field;

public class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
