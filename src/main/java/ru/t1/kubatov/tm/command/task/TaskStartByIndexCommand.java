package ru.t1.kubatov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Start Task by index.";

    @NotNull
    public final static String NAME = "task-start-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(getUserId(), index, Status.IN_PROGRESS);
    }
}
