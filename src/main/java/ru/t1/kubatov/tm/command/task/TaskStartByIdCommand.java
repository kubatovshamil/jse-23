package ru.t1.kubatov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Start Task by ID.";

    @NotNull
    public final static String NAME = "task-start-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(getUserId(), id, Status.IN_PROGRESS);
    }
}
