package ru.t1.kubatov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    public final static String DESCRIPTION = "Update Project by ID.";

    @NotNull
    public final static String NAME = "project-update-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        getProjectService().updateById(getUserId(), id, name, description);
    }
}
